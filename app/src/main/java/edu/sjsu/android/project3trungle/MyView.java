package edu.sjsu.android.project3trungle;

import static android.view.Surface.ROTATION_0;
import static android.view.Surface.ROTATION_180;
import static android.view.Surface.ROTATION_270;
import static android.view.Surface.ROTATION_90;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.view.View;


public class MyView extends View implements SensorEventListener {

    // You can change the ball size if you want
    private static final int BALL_SIZE = 200;
    private Bitmap field;
    private final Bitmap ball;
    private float XOrigin;
    private float YOrigin;
    private float horizontalBound;
    private float verticalBound;
    private final Particle mBall = new Particle();
    // Paint object is used to draw your name
    private final Paint paint = new Paint();

    private final SensorManager mSensorManager;
    private Sensor mSensor;
    int rotation;
    private float x,y,z;
    private long timestamp;

    // set attributes (objects) needed for sensor
    // HINT: 2 of them are classes in the sensor framework
    //      1 is used for getting the rotation from "natural" orientation
    //      4 of them are used for the sensor data (3 axes + timestamp)

    public MyView(Context context) {
        super(context);

        // You will see errors here because there are no image files yet.
        // Add the images under drawable folder to get rid of the errors.
        Bitmap b = BitmapFactory.decodeResource(getResources(), R.drawable.basketball);
        ball = Bitmap.createScaledBitmap(b, BALL_SIZE, BALL_SIZE, true);
        field = BitmapFactory.decodeResource(getResources(), R.drawable.field);

        // TODO: Initialize the objects related to the sensor except for sensor data
        mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        if(mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null){
            mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        }
    }

    // set XOrigin, YOrigin, HorizontalBound and VerticalBound based on the screen size
    // HINT: there is a method in View class related to the size of the screen.
    //      The method's parameters include width and height of the screen.
    //      So override that method to set the 4 attributes.
    //      Also, you should createScaledBitmap for the basketball field,
    //      use the width and height of the screen in this method.

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // draw the field and the ball
        canvas.drawBitmap(field, 0, 0, null);
        // "draw" your name (make it easy to see)
        paint.setColor(Color.WHITE);
        paint.setTextSize(75);
        canvas.drawText("TrungLe", 400, 450, paint);

        // control the ball based on the sensor data using methods in Particle class
        mBall.updatePosition(x,y,z, timestamp);
        mBall.resolveCollisionWithBounds(horizontalBound, verticalBound);
        canvas.drawBitmap(ball,
                (XOrigin - BALL_SIZE / 2f) + mBall.mPosX,
                (YOrigin - BALL_SIZE / 2f) - mBall.mPosY, null);
        // view will call invalidate(), the view's appearance may need to be changed.
        invalidate();
    }

    public void startSimulation() {
        if(mSensor != null){
            mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_UI);
        }
    }

    public void stopSimulation() {
        mSensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        // get the sensor data (set them to the corresponding class attributes)
        // Remember to interpret the data as discussed in Lesson 14 page 16.
        if(sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            switch (rotation){
                case ROTATION_0:
                    x = sensorEvent.values[0];
                    y = sensorEvent.values[1];
                    break;
                case ROTATION_90:
                    x = -sensorEvent.values[1];
                    y = sensorEvent.values[0];
                    break;
                case ROTATION_180:
                    x = -sensorEvent.values[0];
                    y = -sensorEvent.values[1];
                    break;
                case ROTATION_270:
                    x = sensorEvent.values[0];
                    y = -sensorEvent.values[1];
                    break;
            }
            z = sensorEvent.values[2];
            timestamp = System.nanoTime();
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        horizontalBound = (right - left)/2f - BALL_SIZE/2f;
        verticalBound = (bottom - top)/2f - BALL_SIZE/2f;
        XOrigin = (right - left)/2f;
        YOrigin = (bottom - top)/2f;
        field = Bitmap.createScaledBitmap(field, right - left, bottom - top, true);
    }
}